using System;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using Gtk;
using GtkForms;

namespace Metis
{
	public enum BlockMaterial {
		Steel, Tungsten, Ceramic, Mixed
	}

	public enum BlockType {
		Metric, Imperial
	}

	[Serializable]
	public class BlockSetStore : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		#region Bound Members

		private string caltrakID = "";
		private string make = "";
		private string model = "";
		private string owner = "";
		private string serial = "";
		private string markings = "";
		private string description = "";

		private DateBuffer calibrationDate = new DateBuffer (){Date = DateTime.Today.ToShortDateString()};
		private DateBuffer recalibrationDate = new DateBuffer (){Date = DateTime.Today.AddYears (4).ToShortDateString()};
		private NotifiedBindingList<string> reports = new NotifiedBindingList<string> ();
		private BlockMaterial material = BlockMaterial.Steel;
		private BlockType type = BlockType.Metric;

		#endregion

		#region Tied fields

		public string CaltrakID {
			get { return caltrakID; }
			set {
				if (caltrakID != value) {
					caltrakID = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("CaltrakID"));
				}
			}
		}

		public string Description {
			get { return description; }
			set {
				if (description != value) {
					description = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Description"));
				}
			}
		}

		public string Owner {
			get { return owner; }
			set {
				if (owner != value) {
					owner = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Owner"));
				}
			}
		}

		public string Make {
			get { return make; }
			set {
				if (make != value) {
					make = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Make"));
				}
			}
		}

		public string Model {
			get { return model; }
			set {
				if (model != value) {
					model = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Model"));
				}
			}
		}

		public string Serial {
			get { return serial; }
			set {
				if (serial != value) {
					serial = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Serial"));
				}
			}
		}

		public string Markings {
			get { return markings; }
			set {
				if (markings != value) {
					markings = value;
					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Markings"));
				}
			}
		}

		//Doesn't need to be an INotify object. We never programatically set the date.
		public class DateBuffer : INotifyPropertyChanged
		{
			public event PropertyChangedEventHandler PropertyChanged;

			private DateTime date;
			private string dateBuf;

			public bool DateValid { get; set; }

			public string Date {
				get { return dateBuf; }
				set {
					if (dateBuf == value)
						return;

					try {
						dateBuf = value;
						date = DateTime.Parse (value);
						DateValid = true;
					} catch (FormatException) {
						date = DateTime.MinValue;
						DateValid = false;
					}

					if (PropertyChanged != null)
						PropertyChanged (this, new PropertyChangedEventArgs ("Date"));
				}
			}

			public void RecalculateDate ()
			{
				if (DateValid)
					Date = date.ToShortDateString();
			}
		}

		public DateBuffer CalibrationDate { 
			get { return calibrationDate; } 
			set { calibrationDate = value; } 
		}

		public DateBuffer RecalibrationDate { 
			get { return recalibrationDate; } 
			set { recalibrationDate = value; } 
		}

		public NotifiedBindingList<string> Reports { 
			get { return reports; } 
			set { reports = value; } 
		}

		public BlockMaterial Material {
			get { return material; } 
			set { material = value; }
		}

		public BlockType Type {
			get { return type; } 
			set { type = value; }
		}
	
		#endregion

		public BlockSetStore ()
		{
		}

		public List<BlockNode> BlocksList {
			get { 
				var retval = new List<BlockNode>(); 
				blocks.Foreach(delegate(TreeModel model, TreePath path, TreeIter iter) {
					retval.Add(model.GetValue(iter, 0) as BlockNode);
					return false;
				});
				return retval;
			}
			set { 
				foreach (var block in value) 
					blocks.AppendValues(block); 
			}
		}

		[NonSerialized]
		private ListStore blocks = new ListStore(typeof(BlockNode));
		[XmlIgnore]
		public ListStore Blocks {get {return blocks;}}

		private XmlSerializer serializer = new XmlSerializer (typeof(BlockSetStore));
		private string path = "Test.xml";
		public bool CanSave {
			get {
				return path.Length != 0;
			}
		}
		
		public void Save ()
		{
			if (path.Length != 0) {
				using (TextWriter writer = new StreamWriter(path)) {
					serializer.Serialize (writer, this);
					writer.Close ();
				}
			}
		}
	}
}

