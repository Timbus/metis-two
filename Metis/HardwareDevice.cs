using System;
using System.IO.Ports;
using System.Threading;

namespace Metis {

	public delegate void ReturnCallback(string s);

	public static class HardwareDevice {

		private static SerialPort device;
		private static Thread deviceThread;
		private static ReturnCallback returnCallback;

		public static void Create (ReturnCallback rcb) {
			//"COM1:1200,E,7,2"
			if (device == null) {

				device = new SerialPort ("/dev/ttyUSB0", 1200, Parity.Even, 7, StopBits.Two);
				//device = new SerialPort ("COM1", 1200, Parity.Even, 7, StopBits.Two);
				device.NewLine = "\r";
				device.ReadTimeout = 250;
				device.Open ();
				
				deviceThread = new Thread (new ThreadStart (PollSerialPort));
				deviceThread.Start ();

				returnCallback = rcb;
			}
		}

		public static void Destroy () {
			device.Close();
			device = null;
		}


		static void PollSerialPort () {
			while (device.IsOpen) {
				string data;
				try {
					data = device.ReadLine ();
				} catch (TimeoutException) {
					continue;
				} catch (System.IO.IOException) {
					//Called if device.Close is called during a read. 
					//Seems to happen in .net, not in mono.
					break;
				}

				data = data.Trim();
				
				if (data.Length <= 0) {
					//Blank line, or possibly the port died somehow.
					continue;
				}
				Gtk.Application.Invoke (delegate {returnCallback(data);});
			}
		}
	}
}

