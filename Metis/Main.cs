using System;
using System.Resources;
using Gtk;

namespace Metis
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			if ( Environment.OSVersion.Platform != PlatformID.Unix ) {
				Environment.SetEnvironmentVariable(
					"GTK_PATH", "./;" + 
					Environment.GetEnvironmentVariable("GTK_PATH")
				);
			}
			Gtk.Rc.Parse("gtkrc");
			
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
