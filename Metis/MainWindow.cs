using System;
using Gtk;
using System.Collections.Generic;
using GtkForms;

namespace Metis
{
	public partial class MainWindow : GtkForms.FormsWindow
	{	

		private TreeViewColumn MaterialColumn;

		BlockSetStore data = new BlockSetStore();
		ListStore MaterialList = new ListStore(typeof (string));

		public MainWindow () : base (Gtk.WindowType.Toplevel)
		{
			Build ();

			#region View Setup

			var model = data.Blocks;

			//if (data.Reports.Count == 0)
			//	BeginReportEntry(false);

			model.RowDeleted += OnRowChanged;
			model.RowInserted += OnRowChanged;
			model.AppendValues (new BlockNode ());
			
			uiBlockTreeView.Model = model;
			uiBlockTreeView.Selection.Mode = SelectionMode.Multiple;
			
			CellRendererText cr;
			TreeViewColumn tc;
			
			cr = new Gtk.CellRendererText ();
			cr.Editable = true;
			cr.Edited += NominalEdited;
			cr.EditingStarted += NominalEditingStarted;
			tc = uiBlockTreeView.AppendColumn ("Nominal Size", cr);
			tc.Resizable = true;
			tc.Expand = true;
			tc.Data ["Prop"] = "NominalSize";
			tc.SetCellDataFunc (cr, RenderText);
			
			cr = new Gtk.CellRendererText ();
			cr.Editable = true;
			cr.Edited += SerialEdited;
			cr.EditingStarted += SerialEditingStarted;
			tc = uiBlockTreeView.AppendColumn ("Serial", cr);
			tc.Resizable = true;
			tc.Expand = true;
			tc.Data ["Prop"] = "Serial";
			tc.SetCellDataFunc (cr, RenderText);
			
			cr = new Gtk.CellRendererText ();
			cr.Editable = true;
			cr.Edited += DeviationEdited;
			tc = uiBlockTreeView.AppendColumn ("Deviation (nm)", cr);
			tc.Resizable = true;
			tc.Expand = true;
			tc.Data ["Prop"] = "Deviation";
			tc.SetCellDataFunc (cr, RenderText);
			
			var crc = new Gtk.CellRendererCombo ();
			crc.TextColumn = 0;
			crc.Editable = true;
			crc.Edited += MaterialEdited;
			crc.Model = MaterialList;
			MaterialColumn = new TreeViewColumn ("Material", crc);
			MaterialColumn.Resizable = true;
			MaterialColumn.Expand = true;
			MaterialColumn.SetCellDataFunc (crc, RenderComboText);
			MaterialColumn.Data ["Prop"] = "Material";

			foreach (string mat in Enum.GetNames(typeof(BlockMaterial))) {
				if (mat != "Mixed")
					MaterialList.AppendValues(mat);
			}

			#endregion

			#region Data Binding
			//Not sure if this is needed? All examples use it though.
			BindingContext = new BindingContext();

			uiReportSelector.DataSource = data.Reports;
			(uiReportSelector.Cells[0] as CellRendererText).Editable = false;

			uiCaltrakID.DataBindings.Add ("Text", data, "CaltrakID", false, DataSourceUpdateMode.OnPropertyChanged);
			uiCalibrationDate.DataBindings.Add ("Text", data.CalibrationDate, "Date", false, DataSourceUpdateMode.OnPropertyChanged);
			uiRecalibrationDate.DataBindings.Add ("Text", data.RecalibrationDate, "Date", false, DataSourceUpdateMode.OnPropertyChanged);

			uiSetDescription.DataBindings.Add ("Text", data, "Description", false, DataSourceUpdateMode.OnPropertyChanged);
			uiSetMake.DataBindings.Add ("Text", data, "Make", false, DataSourceUpdateMode.OnPropertyChanged);
			uiSetModel.DataBindings.Add ("Text", data, "Model", false, DataSourceUpdateMode.OnPropertyChanged);
			uiSetOwner.DataBindings.Add ("Text", data, "Owner", false, DataSourceUpdateMode.OnPropertyChanged);
			uiSetSerial.DataBindings.Add ("Text", data, "Serial", false, DataSourceUpdateMode.OnPropertyChanged);
			uiSetMarkings.DataBindings.Add ("Text", data, "Markings", false, DataSourceUpdateMode.OnPropertyChanged);

			uiSteelMaterial.DataBindings.Add(new BooleanBinding<BlockMaterial> ("Checked", data, "Material", BlockMaterial.Steel));
			uiTungstenMaterial.DataBindings.Add(new BooleanBinding<BlockMaterial> ("Checked", data, "Material", BlockMaterial.Tungsten));
			uiCeramicMaterial.DataBindings.Add(new BooleanBinding<BlockMaterial> ("Checked", data, "Material", BlockMaterial.Ceramic));
			uiMixedMaterial.DataBindings.Add(new BooleanBinding<BlockMaterial> ("Checked", data, "Material", BlockMaterial.Mixed));

			uiSetMetric.DataBindings.Add(new BooleanBinding<BlockType> ("Checked", data, "Type", BlockType.Metric));
			uiSetImperial.DataBindings.Add(new BooleanBinding<BlockType> ("Checked", data, "Type", BlockType.Imperial));

			#endregion
		}

		protected BlockNode GetBlock (TreePath path)
		{
			var model = data.Blocks;
			TreeIter iter;
			model.GetIter (out iter, path);
			return (BlockNode)model.GetValue (iter, 0);
		}

		protected void NominalEdited (object o, EditedArgs args)
		{
			var path = new TreePath (args.Path);
			double val = 0;
			try {
				val = double.Parse (args.NewText);
			} catch (FormatException) {
			}
			GetBlock (path).NominalSize = val;
		}		

		protected void NominalEditingStarted (object o, EditingStartedArgs args)
		{
			(args.Editable as Entry).Activated += delegate(object sender, EventArgs e) {
				var path = new TreePath (args.Path);
				path.Next();
				uiBlockTreeView.SetCursor(path, uiBlockTreeView.Columns[0], true);
			};
		}

		protected void SerialEdited (object o, EditedArgs args)
		{
			GetBlock (new TreePath(args.Path)).Serial = args.NewText;
		}

		protected void SerialEditingStarted (object o, EditingStartedArgs args)
		{
			(args.Editable as Entry).Activated += delegate(object sender, EventArgs e) {
				var path = new TreePath (args.Path);
				path.Next();
				uiBlockTreeView.SetCursor(path, uiBlockTreeView.Columns[1], true);
			};
		}

		protected void DeviationEdited (object o, EditedArgs args)
		{
			int val = 0; 
			try { 
				val = int.Parse (args.NewText);
			} catch (FormatException) {
			} 
			GetBlock (new TreePath(args.Path)).Deviation = val;
		}

		void MaterialEdited (object o, EditedArgs args)
		{
			try { 
				GetBlock (new TreePath(args.Path)).Material = (BlockMaterial)Enum.Parse(typeof(BlockMaterial), args.NewText);			
			} catch (ArgumentException) {
			}
		}

		protected void RenderText (TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
		{
			var property = (string)tree_column.Data ["Prop"];
			var block = (BlockNode)tree_model.GetValue (iter, 0);

			(cell as Gtk.CellRendererText).Text = typeof(BlockNode).GetProperty (property).GetValue (block, null).ToString ();
		}

		protected void RenderComboText (TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
		{
			var property = (string)tree_column.Data ["Prop"];
			var block = (BlockNode)tree_model.GetValue (iter, 0);

			(cell as Gtk.CellRendererCombo).Text = typeof(BlockNode).GetProperty (property).GetValue (block, null).ToString ();
		}

		protected void OnRowChanged (object o, object args)
		{
			var count = (o as ListStore).IterNChildren ();
			BlockCount.Text = string.Format ("{0} block{1} in set", count, count != 1 ? "s" : "");
		}

		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			data.Save();
			Application.Quit ();
			a.RetVal = true;
		}

		protected void OnAddBlockClicked (object sender, EventArgs e)
		{
			var model = data.Blocks;

			var rows = model.IterNChildren ();
			//No rows? Add a new blank one and quit.
			if (rows == 0) {
				//Add a blank block
				model.AppendValues (new BlockNode ());
				return;
			}
			
			//Otherwise we duplicate either the last selected value 
			//or copy the bottom-most row if no selection.
			TreeIter iter;
			var selection = uiBlockTreeView.Selection;
			var selected = selection.GetSelectedRows ();
			if (selected.Length == 0) {
				model.IterNthChild (out iter, rows - 1);
			} else {
				var btm = selected [selected.Length - 1];
				model.GetIter (out iter, btm);
			}
						
			//Update with a duplicate of the highlighted row.
			var new_iter = model.InsertAfter (iter);
			var node = (BlockNode)model.GetValue (iter, 0);
			model.SetValue (new_iter, 0, node.Clone ());
		}

		protected void OnInsertBatchClicked (object sender, EventArgs e)
		{
			var from = FromSize.Value;
			var to = ToSize.Value;
			var step = StepSize.Value;

			var material = data.Material;
			if (material == BlockMaterial.Mixed)
				material = BlockMaterial.Steel;

			for (double i = from; i < to; i += step) {
				data.Blocks.AppendValues (new BlockNode (){
					NominalSize=i,
					Serial="???",
					Material=material,
				});
			}
		}

		protected void OnRemoveBlockClicked (object sender, EventArgs e)
		{
			var selection = uiBlockTreeView.Selection;

			var treerefs = new List<TreeRowReference> ();
			selection.SelectedForeach ((model,path,iter) => {treerefs.Add (new TreeRowReference (model, path));});
			
			treerefs.ForEach (delegate (TreeRowReference tr) {
				TreeIter it;
				tr.Model.GetIter (out it, tr.Path);
				((ListStore)tr.Model).Remove (ref it);
			});
		}

		protected void OnTypeChanged (object sender, EventArgs e)
		{
			if (uiSetMetric.Active)
				uiBlockTreeView.Columns [2].Title = "Deviation (nm)";
			else
				uiBlockTreeView.Columns [2].Title = "Deviation (uin)";
		}

		private bool hasMaterialColumn = false;

		protected void OnMaterialChanged (object sender, EventArgs e)
		{
			if (uiMixedMaterial.Active) {
				uiBlockTreeView.AppendColumn (MaterialColumn);
				hasMaterialColumn = true;
			} else if (hasMaterialColumn == true) {
				uiBlockTreeView.RemoveColumn (MaterialColumn);
				hasMaterialColumn = false;
			}
		}

		protected void ValidateDate (object sender, EventArgs e)
		{
			var entry = (FormsEntry)sender;
			var buf = (BlockSetStore.DateBuffer)entry.DataBindings[0].DataSource;
			if (buf.DateValid) {
				entry.ModifyBase (StateType.Normal, new Gdk.Color (200, 255, 200));
			} else {
				entry.ModifyBase (StateType.Normal, new Gdk.Color (255, 200, 200));
			}

			buf.RecalculateDate();
		}

		protected void OnDeleteReportClicked (object sender, EventArgs e)
		{
			var msgbox = new MessageDialog(
				this, 
				DialogFlags.Modal, 
				MessageType.Warning, ButtonsType.YesNo, 
				"<b>Really delete this report?</b>"
			);
			msgbox.SecondaryText = "This is usually a bad idea! Only do it if you are correcting a typo or something.";
			ResponseType reply = (ResponseType)msgbox.Run ();
			msgbox.Destroy();

			if (reply == ResponseType.Yes) {
				var idx = uiReportSelector.SelectedIndex;
				data.Reports.RemoveAt(idx);
			}
		}

		protected void OnAddReportClicked (object sender, EventArgs e)
		{
			BeginReportEntry(true);
		}
		
		string initialReportText = "";
		protected void BeginReportEntry (bool stealFocus)
		{
			uiReportSelector.Hide ();
			uiNewReportEntry.Show ();

			if (stealFocus)
				uiNewReportEntry.HasFocus = true;

			uiNewReportEntry.Text = initialReportText = string.Format ("{0:yy}ML-", DateTime.Now);
			uiNewReportEntry.Position = -1;
		}

		protected void EndReportEntry () {
			if (!(uiNewReportEntry.Text.Length == 0 || uiNewReportEntry.Text == initialReportText)) {
				var reportnum = uiNewReportEntry.Text;
				uiNewReportEntry.Text = "";
				data.Reports.Insert (0, reportnum);
			}
			uiNewReportEntry.Hide ();
			uiReportSelector.SelectedIndex = 0;
			uiReportSelector.Show ();
		}

		protected void OnReportEntryActivated (object o, EventArgs args)
		{
			EndReportEntry ();
		}

		protected void OnReportEntryUnfocused (object o, FocusOutEventArgs args)
		{
			EndReportEntry ();
		}

		protected void OnExecuteTest (object sender, EventArgs e)
		{
			new BlockTestWindow().Show();
		}

	}

}