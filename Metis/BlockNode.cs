using System;
using System.Linq;
using System.Xml.Serialization;

namespace Metis
{
	[Serializable]
	public class BlockNode {

		public double NominalSize     { get; set; }
		public string Serial          { get; set; }
		public BlockMaterial Material { get; set; }


		public double[] DeviationTests { get; set; }
		[XmlIgnore]
		public double   Deviation      { 
			get {
				var deviation = 
					(DeviationTests[1] + DeviationTests[2]) / 2 -
					(DeviationTests[0] + DeviationTests[3]) / 2;

				return deviation; // + referror;
			}

			set {
				DeviationTests = new double[]{0, value, value, 0};
			}
		}

		public double[] FlatnessTests { get; set; }
		[XmlIgnore]
		public double   Flatness      { 
			get {
				return FlatnessTests.Max() - FlatnessTests.Min();
			}

			set {
				FlatnessTests = new double[]{value, 0};
			}
		}
		
		public BlockNode ()
		{
			NominalSize = 0;
			Serial = "???";
			DeviationTests = new double[]{0,0,0,0};
			FlatnessTests = new double[]{0,0,0,0,0};
			Material = BlockMaterial.Steel;
		}

		public BlockNode Clone ()
		{
			return (BlockNode)this.MemberwiseClone ();
		}

	}
}

