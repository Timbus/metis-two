using System;

namespace Metis
{
	public partial class BlockTestWindow : Gtk.Window
	{

		public BlockTestWindow () : base(Gtk.WindowType.Toplevel) {
			this.Build ();

			//Set the tab order so taking input from serial is much easier.
			uiFlatnessEntries.FocusChain = new Gtk.Widget[]{
				uiCentreEntry,
				uiBottomLeftEntry,
				uiTopLeftEntry,
				uiTopRightEntry,
				uiBottomRightEntry
			};

			HardwareDevice.Create(onInput);
		}

		protected void onInput (string input) {
			var focused = this.Focus;
			var type = focused.GetType ();
			if (type == typeof(Gtk.Entry) || type == typeof(GtkForms.FormsEntry)) {
				(focused as Gtk.Entry).Text = input;
			}
			this.ChildFocus (Gtk.DirectionType.TabForward);
		}

		protected void OnDeleted (object o, Gtk.DeleteEventArgs args) {
			HardwareDevice.Destroy();
		}
	}
}

